class carriageDriver {
  getUsername(){
    return this.userName;
  }
  setUsername(userName) {
    if (!isValidName(userName)){
      throw new Error("SQL Invalid constraint.");
    }
    this.userName = userName;
  }

  getFirstname(){
    return this.firstName;
  }
  setFirstname(firstName){
    if(!isValidName(firstName)){
      throw new Error("SQL Invalid constraint.");
    }
    this.firstName = firstName;
  }

  getMiddlename(){
    return this.middleName;
  }
  setMiddlename(middleName){
    if(!isValidName(middleName)){
      throw new Error("SQL Invalid constraint.");
    }
  }

  getLastname(){
    return this.lastName;
  }
  setLastname(lastName){
    if(!isValidName(lastName)){
      throw new Error("SQL Invalid constraint.");
    }
  }

  getStartingDate(){
    return this.startingDate;
  }
  setStartingDate(startingDate){
    if (startingDate >= this.endingDate && this.endingDate ){
      throw new Error("Invalid Date");
    }
    this.startingDate = startingDate;
  }

  getEndingDate(){
    return this.endingDate;
  }
  setEndingDate(endingDate){
    if (endingDate <= this.startingDate && this.startingDate) {
      throw new Error("Invalid Date");
    }
    this.endingDate = endingDate;
  }

  getMaxCapacity() {
    return this.maxCapacity;
  }
  setMaxCapacity(maxCapacity) {
    if (!isNaN(maxCapacity) && maxCapacity <= 800) {
      this.maxCapacity = maxCapacity;
    } else {
      throw new Error("There is not enough space");
    }
  }

  getWeightType() {
    return this.weightType;
  }
  setWeightType(weightType) {
    if(!this.availableTypes[weightType]) {
      throw new Error("Invalid type");
    } 
    this.weightType = this.availableTypes[weightType];
  }
  availableTypes = {
    heavy: 'Heavy',
    megaHeavy: 'Mega_Heavy',
    gigaHeavy: 'Giga_Heavy'
  }
}

function isValidName(incomingStringToTest)
{
  let validLetters = new RegExp(/^[a-zA-Z]{1,20}$/);
  return validLetters.test(incomingStringToTest);
}

function createUser() {
  var c = new carriageDriver();
  try {
    c.getUsername();
    c.setUsername("saqweddasdweqeqasdq");
    c.getUsername();
    c.setLastname("dasdasda");
    c.getFirstname();
    
    c.setEndingDate(new Date(2001, 11, 23));
    c.getEndingDate();
    c.setStartingDate(new Date(2000, 01, 02));
    c.getStartingDate();
   
    c.setMaxCapacity(201);
    c.getMaxCapacity();

    c.setWeightType("heavy");
    c.getWeightType();  
  } catch (e) {
    console.error(e.message);
    c = null;
  } finally {
    console.log('Finally done');
  }
}
createUser();


